"""Tests for openai module."""
import pytest
import sys,os

# add path to parent directory to sys.path
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))

from text_to_embedding import file
from text_to_embedding.openai import text_to_vector, extract_embedding_response
"""
@pytest.mark.parametrize(
    ("text", "expected"),
    [
        ("Anika", "Hello Anika!"),
        ("Isabella", "Hello Isabella!"),
    ],
)
def test_text_to_vector(text, expected):
    assert text_to_vector(text) == expected

"""

def test_extract_embedding_response():
    test_data_file = os.path.join(os.path.dirname(__file__), 'data_openai.json')
    result = file.load_file_type(test_data_file)
    extracted = extract_embedding_response(result)
    assert len(extracted) == 1536


def test_text_to_vector_same_size():
    vector1 = text_to_vector("Anika")
    assert len(vector1) == 1
    assert len(vector1[0]) == 1536
    vector2 = text_to_vector("Peter")
    assert len(vector2) == 1
    assert len(vector2[0]) == 1536
    # different data
    assert vector1 != vector2
