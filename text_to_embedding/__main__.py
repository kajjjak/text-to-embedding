# type: ignore[attr-defined]
from typing import Optional

from enum import Enum
from random import choice

import typer
from rich.console import Console

from text_to_embedding import version
from text_to_embedding.embed import embed_text


class Model(str, Enum):
    text_embedding_ada_002 = "text-embedding-ada-002"


app = typer.Typer(
    name="text_to_embedding",
    help="Create vector from text allowing tags, source, section and category to be added as well",
    add_completion=False,
)
console = Console()


def version_callback(print_version: bool) -> None:
    """Print the version of the package."""
    if print_version:
        console.print(f"[yellow]text_to_embedding[/] version: [bold blue]{version}[/]")
        raise typer.Exit()


@app.command(name="")
def main(
    input: str = typer.Option(..., help="File to load."),
    output: Optional[str] = typer.Option(None, "-i", "--output", help="File to save."),
    # array of strings
    model: Optional[Model] = typer.Option(
        None,
        "-m",
        "--model",
        case_sensitive=False,
        help="Model to use for embedding.",
    ),
    verbose: Optional[bool] = typer.Option(
        None,
        "--verbose",
        help="Verbose run showing all the nitty gritty.",
    ),
    print_version: bool = typer.Option(
        None,
        "-v",
        "--version",
        callback=version_callback,
        is_eager=True,
        help="Prints the version of the text_to_embedding package.",
    ),
) -> None:
    """Text embedding"""

    # handle model
    if model is None:
        model = choice(list(Model))

    # handle verbose
    if verbose:
        verbose = console.print
    else:
        verbose = None
    # run the code
    try:
        embed_text(input, model, output, verbose=verbose)
        console.print(f"[bold green]Success[/]")
    except Exception as e:
        console.print(f"[bold red]Error[/] {e}")


if __name__ == "__main__":
    app()
