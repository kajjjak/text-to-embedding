"""Using OpenAI's Embedding API to convert text to a vector representation.

Max tokens: 8192

Here are some helpful rules of thumb for understanding tokens in terms of lengths:

    - 1 token ~= 4 chars in English > ~2000 words
    - 1 token ~= ¾ words-
    - 100 tokens ~= 75 words

Or

    - 1-2 sentence ~= 30 tokens     > ~250 sentences
    - 1 paragraph ~= 100 tokens     > ~82 paragraphs
    - 1,500 words ~= 2048 tokens

See more on tokens
    https://beta.openai.com/docs/api-reference/retrieve-engine-api#retrieve-engine-api-response-formathttps://help.openai.com/en/articles/4936856-what-are-tokens-and-how-to-count-them

"""

import os
import openai
from text_to_embedding import chunker

openai.api_key = os.getenv("OPENAI_API_KEY")

def extract_embedding_response(result):
    # check if result is a dict
    if not isinstance(result, dict):
        raise TypeError("Returned result was not a dict")

    # make sure we got all the result
    if result["usage"]["prompt_tokens"] != result["usage"]["total_tokens"]:
        raise ValueError("Total token count was not consumed")

    # check if result has a data key
    if "data" in result and isinstance(result["data"], list):
        vector = result["data"][0]
        return vector["embedding"]

def _text_to_vector(text: str, model_name: str="text-embedding-ada-002"):
    """
    Helper function for text_to_vector where text is a string of text, returns a vector representation of the text.
    Must not exceed 8192 tokens. See above for details on tokens and how to count them.

    Args:
        text (str): Text to convert to vector must not exceed 8192 tokens

    Returns:
        floats: array of 46705 floats representing the vector representation of the text
    """

    result = openai.Embedding.create(
        model=model_name,
        input=text
    )

    # validate the result
    vector = extract_embedding_response(result)
    return vector


def text_to_vector(text: str, model_name: str="text-embedding-ada-002", verbose=None):
    """Where text is a string of text, returns a vector representation of the text.

    See _text_to_vector for more details

    Will split text into chunks of 8192 tokens calling _text_to_vector on each chunk
    """

    # throw an error if text is not a string
    if not isinstance(text, str):
        raise TypeError("Text must be a string")


    # split text into chunks of 8192 tokens
    chunks = chunker.split_text(text, 100, 4)
    text_length = len(text)
    text_embeded = 0
    if verbose: print("Number of chunks {} content length {}".format(len(chunks), text_length))

    # vectorize each chunk
    vectors = []
    for chunk in chunks:
        chunk_length = len(chunk)
        if verbose: print("Chunk size: {0:.0%}% {1}/{2}".format(text_embeded/text_length, chunk_length, text_length))
        text_embeded += chunk_length
        vector = _text_to_vector(chunk, model_name)
        vectors.append(vector)

    if verbose: print("Chunk size: 100% complete")

    return vectors
