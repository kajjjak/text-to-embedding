"""Loads and saves files
"""

import os
import re
import json


def load_file(filename, verbose=None):
    """
    Loads a file and returns its contents.
    Args:
        filename (str): Path to file.
    """

    if verbose: verbose("Opening file: {}".format(filename))
    # load file assuming text
    with open(filename, "r") as f:
        if verbose: verbose("Loading file: {}".format(filename))
        return f.read()


def load_file_type(filename, verbose=None):
    """
    Loads a file and returns its contents.
    Format is determined by the file extension.

    Args:
        filename (str): Path to file.

    """
    # get file extension
    extension = re.search(r"\.([a-z]+)$", filename).group(1)

    # load file
    if extension == "txt":
        return load_file(filename, verbose)
    elif extension == "json":
        return load_json(filename, verbose)
    else:
        raise ValueError("Unsupported file extension: {}".format(extension))


def load_json(filename, verbose=None):
    """Loads json file and returns its contents.

    Args:
        filename (str): Name of the file to load
    """
    if verbose: verbose("Opening file: {}".format(filename))
    with open(filename, "r") as f:
        if verbose: verbose("Loading json file: {}".format(filename))
        return json.load(f)


def save_file(filename, contents, verbose=None):
    """Save a file and its contents returning the number of bytes written or 0 if failed.
    Generate the file path if it does not exist.

    Args:
        filename (str): Path to the file to be saved. Generating the missing directories
        contents (str): Contents to be saved.
    """
    # generate path if it does not exist
    path = os.path.dirname(filename)
    if not os.path.exists(path) and path != "":
        if verbose: verbose("Creating path: {}".format(path))
        os.makedirs(path)

    # save file
    try:
        if verbose: verbose("Saving file: {}".format(filename))
        with open(filename, "w") as f:
            if verbose: verbose("Writing file: {}".format(filename))
            return f.write(contents)
    except:
        return 0
