"""Embedding module.
"""
import re
from text_to_embedding import file
from text_to_embedding import openai

def embed_text(file_input, model, file_output=None, verbose=None):
    """Loads file and embeds text. Then writes the result to a file.

    Args:
        file_input (str): File input path
        model (str): Name of the mode
        file_output (str, optional): File output name. Defaults to the same name of file postfixing "embeding" before the extension.
    """
    # load file
    text = file.load_file(file_input, verbose=verbose)

    # embed text
    embedding = openai.text_to_vector(text, model_name=model, verbose=verbose)

    # create save file name
    if file_output is None:
        # get file extension allowing for no extension
        extension = re.search(r"\.([a-z]+)$", file_input).group(1) if "." in file_input else ""
        # generate file name
        file_output = file_input.replace(
            ".{}".format(extension), "-embedding.{}".format(extension)
        )
        # if the file is missing an extension then just add -embedding to the end
        file_output = file_output.replace("..", ".") + "-embedding"
        if verbose: verbose("Generated file name: {}".format(file_output))

    # conver the array of floats to a string with new lines, removing the brackets
    text_embedding = "\n".join([str(x) for x in embedding]).replace("[", "").replace("]", "")

    # save to file
    file.save_file(file_output, text_embedding, verbose=verbose)

