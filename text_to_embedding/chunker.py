"""
Loads large text files and splits them into chunks allowing some overlapping.
"""


def chunk_content(content, chunk_size, overlap_size):
    """Checks if a file exists and its size and determins if it should be chunked.

    Args:
        content (str): The text content to chunk
        chunk_size (int): The size of the chunks
        overlap_size (int): The amount of sentances to overlap
    """
    if len(content) > chunk_size:
        chunks = []
        sentances = content.split(". ")
        for i in range(0, len(sentances), chunk_size - overlap_size):
            chunks.append(". ".join(sentances[i : i + chunk_size]))
        return chunks
    else:
        return [content]


def chunk_sentances(content, sentance_count, overlap_size):
    """Checks if a file exists and its size and determins if it should be chunked.

    Args:
        content (str): The text content to chunk
        sentance_count (int): The size of the chunks
        overlap_size (int): The amount of sentances to overlap
    """
    sentances = content.split(". ")
    if len(sentances) > sentance_count:
        chunks = []
        for i in range(0, len(sentances), sentance_count - overlap_size):
            chunks.append(". ".join(sentances[i : i + sentance_count]))
        return chunks
    else:
        return [content]



# By ChatGTP
# Can you create python function that
def split_sentances(sentences, max_sentence_count, overlap_sentence_count):
    """ Splits the text into chunks.
    Takes in text of any size. And splits it into chunks. Where each chunk size may not exceed 8192 tokens. Each token is 4 characters.
    Args:
        sentences (list): List of sentences to split into chunks
        max_sentence_count (int): Maximum number of sentences in each chunk
        overlap_sentence_count (int): Number of sentences to overlap between chunks

    Returns:
        chunks (list): List of chunks of sentences
    """
    chunks = []
    start_index = 0
    end_index = max_sentence_count

    # if sentances is less than max_sentence_count return the sentances
    if len(sentences) < max_sentence_count:
        return [sentences]

    while start_index < len(sentences):
        chunk = sentences[start_index:end_index]
        chunks.append(chunk)
        start_index += (max_sentence_count - overlap_sentence_count)
        end_index += (max_sentence_count - overlap_sentence_count)

    return chunks

def split_text(text, max_sentence_count, overlap_sentence_count):
    """ Splits the text into chunks.

    Args:
        text (str): Text to split into chunks
    """
    # split text into sentences
    sentences = text.split(". ")
    text_splits = []
    # split sentences into chunks
    chunks = split_sentances(sentences, max_sentence_count, overlap_sentence_count)

    # join the chunks back into text
    for chunk in chunks:
        text_splits.append(". ".join(chunk))

    return text_splits
